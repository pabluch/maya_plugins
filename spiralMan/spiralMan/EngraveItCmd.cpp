#include "EngraveItCmd.h"
#include "EngraveItNode.h"
#include "BasicLocator.h"
#include "helpers.h"

#include <maya/MSelectionList.h>
#include <maya/MGlobal.h>
#include <maya/MDagPath.h>
#include <maya/MFnTransform.h>
#include <maya/MItSelectionList.h>
#include <maya/MPlug.h>
#include <maya/MFnNurbsCurve.h>

MStatus EngraveItCmd::doIt ( const MArgList &args )
{ 
	MStatus stat;
	MSelectionList selection;
	MGlobal::getActiveSelectionList( selection );
	MDagPath dagPath;
   MFnNurbsCurve curveFn;
   MObject locatorTransform,newMeshTransform, newMeshShape, locatorNodeObj, engraveNodeObj;
   MFnDependencyNode engraveNodeFn, nodeFn;
   MPlug outMeshPlug, inMeshPlug;
   MObject p1, p2;

   locatorTransform = dagMod[0].createNode("transform");
   locatorNodeObj = dagMod[0].createNode(BasicLocator::typeId, locatorTransform, &stat);
   _CheckStatus(stat, "locator creation failed: " + stat.errorString());
   dagMod[0].renameNode(locatorNodeObj, "engravingLocator");
   MFnDagNode locDepNodeFn(locatorNodeObj);
   engraveNodeObj = dagMod[0].MDGModifier::createNode(EngraveItNode::id, &stat);
   _CheckStatus(stat, "EngraveIt node creation failed: " + stat.errorString());
   engraveNodeFn.setObject(engraveNodeObj);

	MItSelectionList iter( selection, MFn::kNurbsCurve );
   if (iter.isDone())
   {
       MGlobal::displayInfo("Please select a curve node first.");
       return stat;
   }

   for (int i =0; !iter.isDone(); iter.next(),++i)
   {
       iter.getDagPath(dagPath);
       curveFn.setObject(dagPath);
       stat = dagMod[0].connect(curveFn.findPlug("worldSpace").elementByLogicalIndex(0), engraveNodeFn.findPlug("traceCurve").elementByLogicalIndex(i));
       MGlobal::displayInfo("adding curve: "+dagPath.fullPathName());
       _CheckStatus(stat, "Connecting curves in command error: " + stat.errorString());
   }

   newMeshTransform = dagMod[0].createNode("transform");
   newMeshShape = dagMod[0].createNode("mesh", newMeshTransform);
   dagMod[0].doIt();
   
   nodeFn.setObject(newMeshShape);
   engraveNodeFn.setObject(engraveNodeObj);

   p1 = engraveNodeFn.attribute("tracedGeometry", &stat);
   _CheckStatus(stat, "get out mesh plug failed: " + stat.errorString());
   p2 = nodeFn.attribute("inMesh", &stat);
   _CheckStatus(stat, "get in mesh plug failed: " + stat.errorString());
   stat = dagMod[1].MDGModifier::connect(engraveNodeObj , p1, newMeshShape, p2);
   _CheckStatus(stat, "connect tracedGeom failed: " + stat.errorString());
   dagMod[1].renameNode(newMeshTransform, "engravingNode");

   nodeFn.setObject(newMeshShape);
   dagMod[1].commandToExecute(MString("sets -e -fe initialShadingGroup ") + nodeFn.name());
   stat = dagMod[1].MDGModifier::connect(engraveNodeFn.findPlug("aspectRatio"), locDepNodeFn.findPlug("width"));
   _CheckStatus(stat, "connect width failed: " + stat.errorString());
   
   p1 = (MObject)engraveNodeFn.findPlug("aspectRatio1", true,&stat);
   _CheckStatus(stat, "plug1: " + stat.errorString());
   p2 = MFnDependencyNode(locatorNodeObj).attribute("height",&stat);
   _CheckStatus(stat, "plug2: " + stat.errorString());
   stat = dagMod[1].MDGModifier::connect(engraveNodeObj,p1, locatorNodeObj,p2);
   _CheckStatus(stat, "connect height failed: " + stat.errorString());

   p1 = (MObject)engraveNodeFn.findPlug("locatorTX", true, &stat);
   _CheckStatus(stat, "engrave locator tx plug: " + stat.errorString());
   p2 = MFnDependencyNode(locatorTransform).attribute("tx", &stat);
   _CheckStatus(stat, "locator tx plug: " + stat.errorString());
   stat = dagMod[1].MDGModifier::connect(locatorTransform, p2, engraveNodeObj, p1);
   _CheckStatus(stat, "connect tx: " + stat.errorString());

   p1 = (MObject)engraveNodeFn.findPlug("locatorTY", true, &stat);
   _CheckStatus(stat, "engrave locator ty plug: " + stat.errorString());
   p2 = MFnDependencyNode(locatorTransform).attribute("ty", &stat);
   _CheckStatus(stat, "locator ty plug: " + stat.errorString());
   stat = dagMod[1].MDGModifier::connect(locatorTransform, p2, engraveNodeObj, p1);
   _CheckStatus(stat, "connect ty: " + stat.errorString());

   p1 = (MObject)engraveNodeFn.findPlug("locatorSY", true, &stat);
   _CheckStatus(stat, "engrave locator sy plug: " + stat.errorString());
   p2 = MFnDependencyNode(locatorTransform).attribute("sy", &stat);
   _CheckStatus(stat, "locator sy plug: " + stat.errorString());
   stat = dagMod[1].MDGModifier::connect(locatorTransform, p2, engraveNodeObj, p1);
   _CheckStatus(stat, "connect sy: " + stat.errorString());

   p1 = (MObject)engraveNodeFn.findPlug("locatorSX", true, &stat);
   _CheckStatus(stat, "engrave locator sx plug: " + stat.errorString());
   p2 = MFnDependencyNode(locatorTransform).attribute("sx", &stat);
   _CheckStatus(stat, "locator sx plug: " + stat.errorString());
   stat = dagMod[1].MDGModifier::connect(locatorTransform, p2, engraveNodeObj, p1);
   _CheckStatus(stat, "connect sx: " + stat.errorString());

	return dagMod[1].doIt();
}

MStatus EngraveItCmd::undoIt()
{
    for (int i=1;i>=0;i--) dagMod[i].undoIt();
    return MS::kSuccess;
}

MStatus EngraveItCmd::redoIt()
{
    for(int i=0;i<2;i++) dagMod[i].doIt();
    return MS::kSuccess;
}

