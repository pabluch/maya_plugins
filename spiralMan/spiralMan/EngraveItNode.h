#pragma once

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MPlugArray.h>
#include <maya/MPoint.h>
#include <maya/MImage.h>

class EngraveItNode : public MPxNode
{
public:
   static   MTypeId id;
   virtual  MStatus compute( const MPlug& plug, MDataBlock& data );
   virtual  MStatus setDependentsDirty(const MPlug &plugBeingDirtied, MPlugArray &affectedPlugs);
	
   static   void *creator();
	static   MStatus initialize();
   double   sampleImageAt(MPoint pt, unsigned char *pixels, unsigned int pixelStride, int _w, int _h, double _lw, double _lh);
   MMatrix  getWorldToLocatorTrans();
   enum SampleModes { E_ONE, E_NORM, E_INVERSE, E_STORED };

private:
   static MObject resample;
	static MObject traceCurve;
	static MObject traceImageFilename;
   static MObject traceStep;
   static MObject lineWidth;
   static MObject trashold;
   static MObject tracedGeom;
   static MObject aspectRatio;
   static MObject aspectRatio1;
   static MObject tracedPoints;
   static MObject locatorTX, locatorTY;
   static MObject locatorSX, locatorSY;
   static MObject sampledWeights;
   
   static MObject drawPercent;
   static MObject lastPoint;
   static MObject lastPointWeight;

   double _locTX, _locTY;

   SampleModes sampleImageVariants = E_NORM;
};


