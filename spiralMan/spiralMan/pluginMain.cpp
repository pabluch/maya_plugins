#include <maya/MFnPlugin.h>

#include "EngraveItNode.h"
#include "EngraveItCmd.h"
#include "BasicLocator.h"
#include "helpers.h"

MStatus initializePlugin( MObject obj )
{ 
    //_putenv("MAYA_ENABLE_VP2_PLUGIN_LOCATOR_LEGACY_DRAW");
    //_putenv("MAYA_VP2_USE_VP1_SELECTION");

    MStatus stat;
    MFnPlugin plugin( obj, "Pawel Lukaszewicz", "1.0", "Any");

    stat = plugin.registerCommand( "engraveIt", EngraveItCmd::creator );
    _CheckStatusReturn(stat, "register Command failed");
    
    stat = plugin.registerNode( "EngraveItNode", EngraveItNode::id, EngraveItNode::creator, EngraveItNode::initialize );
    _CheckStatusReturn(stat, "register Node failed");
    
    stat = plugin.registerNode(BasicLocator::typeName, BasicLocator::typeId, &BasicLocator::creator, &BasicLocator::initialize, MPxNode::kLocatorNode);
    _CheckStatusReturn(stat, "register Locator failed");

    return stat;
}

MStatus uninitializePlugin( MObject obj)
{
    MStatus stat;
    MFnPlugin plugin( obj );

    stat = plugin.deregisterCommand( "engraveIt" );
    _CheckStatusReturn(stat, "deregister Command failed");

    stat = plugin.deregisterNode(EngraveItNode::id );
    _CheckStatusReturn(stat, "deregister Node failed");
    
    stat = plugin.deregisterNode(BasicLocator::typeId);
    _CheckStatusReturn(stat, "deregister Node failed");
    
    return stat;
}
