#pragma once
#include <maya/MGlobal.h>

inline MString FormatError(const MString &msg, const MString &sourceFile, const int &sourceLine)
{
	MString txt("[engraveIt]");
	txt += msg;
	txt += ", File: ";
	txt += sourceFile;
	txt += " Line: ";
	txt += sourceLine;
	return txt;
}

#define _Error( msg )\
{ \
	MString __txt = FormatError( msg, __FILE__, __LINE__ );\
	MGlobal::displayError(  __txt );\
	cerr << endl << "Error: " << __txt;\
} \

#define _CheckBool( result )\
if( !(result) )\
{ \
	_Error( #result ); \
} \

#define _CheckStatus( stat, msg ) \
if( !stat ) \
{ \
	_Error( msg ); \
}

#define _CheckObject( obj, msg ) \
if( obj.isNul( ) ) \
{ \
	_Error( msg ); \
} \

#define _CheckStatusReturn( stat, msg ) \
if( !stat ) \
{ \
	_Error(msg); \
	return stat; \
}\

