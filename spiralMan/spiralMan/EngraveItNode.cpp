#include "EngraveItNode.h"
#include "helpers.h"
#include "BasicLocator.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MGlobal.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MDistance.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTransform.h>
#include <maya/MPxData.h>
#include <maya/MEulerRotation.h>

MTypeId EngraveItNode::id(0x0012acc0);

MObject EngraveItNode::traceCurve;        
MObject EngraveItNode::traceImageFilename;
MObject EngraveItNode::traceStep;
MObject EngraveItNode::resample;
MObject EngraveItNode::lineWidth;
MObject EngraveItNode::trashold;


MObject EngraveItNode::tracedGeom;
MObject EngraveItNode::aspectRatio;
MObject EngraveItNode::aspectRatio1;

MObject EngraveItNode::locatorTX;
MObject EngraveItNode::locatorTY;
MObject EngraveItNode::locatorSX;
MObject EngraveItNode::locatorSY;

MObject EngraveItNode::sampledWeights;
MObject EngraveItNode::drawPercent;
MObject EngraveItNode::lastPoint;
MObject EngraveItNode::lastPointWeight;
       
MStatus EngraveItNode::setDependentsDirty(const MPlug &plugBeingDirtied, MPlugArray &affectedPlugs)
{
    MString dirty = plugBeingDirtied.partialName();
    if ( dirty.substring(0,9) =="traceCurve" 
        || dirty=="traceImageFilename"
        || dirty=="step" || dirty=="resample" || dirty=="lineWidth" || dirty == "trashold" || dirty == "drawPercent"
        || dirty == "locatorTX" || dirty == "locatorTY" || dirty == "locatorSX" || dirty == "locatorSY" )
    {
        MObject thisNode = thisMObject();
        MPlug p1(thisNode, EngraveItNode::tracedGeom);
        affectedPlugs.append(p1);
        MPlug p2(thisNode, EngraveItNode::lastPoint);
        affectedPlugs.append(p2);
        MPlug p3(thisNode, EngraveItNode::lastPointWeight);
        affectedPlugs.append(p3);
    }
    return(MS::kSuccess);
}

double EngraveItNode::sampleImageAt(MPoint pt, unsigned char *pixels, unsigned int pixelStride, int _w, int _h, double _lw, double _lh)
{
    if(sampleImageVariants == E_NORM || sampleImageVariants == E_INVERSE)
    {
        pt.x += _lw / 2;
        pt.y += _lh / 2;
        if (pt.x < 0 || pt.x > _lw || pt.y <0 || pt.y > _lh) return 1.;
        int w, h;
        w = _w*pt.x / _lw;
        h = _h*pt.y / _lh;

        unsigned char	r = pixels[(w * pixelStride) + _w * (h * pixelStride)];
        double result = r / 255.0f;
        if (sampleImageVariants == E_INVERSE) result = 1. - result;

        return result;
    }
    else return 1.; //E_ONE
}

MStatus EngraveItNode::initialize()
{
    MStatus stat;
    MFnTypedAttribute tAttr;
    traceImageFilename = tAttr.create("traceImageFilename", "traceImageFilename", MFnData::kString, &stat);
    tAttr.setStorable(true);
    MFnStringData fnStringData;
    tAttr.setDefault(fnStringData.create("d:\\in.png"));
    _CheckStatus(stat, (MString("Unable to create attr filename: ") + stat.errorString()))
    addAttribute(traceImageFilename);

    traceCurve = tAttr.create("traceCurve", "traceCurve", MFnData::kNurbsCurve);
    _CheckStatus(stat, (MString("Unable to create curves attr: ") + stat.errorString()))
    tAttr.setArray(true);
    tAttr.setKeyable(true);
    addAttribute(traceCurve);

    tracedGeom = tAttr.create("tracedGeometry", "geo", MFnData::kMesh);
    tAttr.setKeyable(false);
    tAttr.setHidden(true);
    addAttribute(tracedGeom);

    MFnUnitAttribute uAttr;
    traceStep = uAttr.create("traceStep", "step", MFnUnitAttribute::kDistance);
    uAttr.setDefault(MDistance(.1, MDistance::uiUnit()));
    uAttr.setStorable(true);
    uAttr.setKeyable(true);
    addAttribute(traceStep);

    MFnNumericAttribute nAttr;
    aspectRatio = nAttr.create("aspectRatio", "aspect", MFnNumericData::kDouble, 1.0);
    nAttr.setStorable(true);
    nAttr.setKeyable(false);
    nAttr.setHidden(true);
    addAttribute(aspectRatio);

    aspectRatio1 = nAttr.create("aspectRatio1", "aspect1", MFnNumericData::kDouble, 1.0);
    nAttr.setStorable(true);
    nAttr.setKeyable(false);
    nAttr.setHidden(true);
    addAttribute(aspectRatio1);

    trashold = nAttr.create("trashold", "trashold", MFnNumericData::kDouble, .4);
    nAttr.setStorable(true);
    nAttr.setKeyable(false);
    addAttribute(trashold);

    lineWidth = nAttr.create("lineWidth", "lineWidth", MFnNumericData::kDouble, .1);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    addAttribute(lineWidth);

    locatorTX = nAttr.create("locatorTX", "locatorTX", MFnNumericData::kDouble);
    nAttr.setStorable(false);
    nAttr.setKeyable(false);
    nAttr.setHidden(true);
    addAttribute(locatorTX);

    locatorTY = nAttr.create("locatorTY", "locatorTY", MFnNumericData::kDouble);
    nAttr.setStorable(false);
    nAttr.setKeyable(false);
    nAttr.setHidden(true);
    addAttribute(locatorTY);

    locatorSX = nAttr.create("locatorSX", "locatorSX", MFnNumericData::kDouble);
    nAttr.setStorable(false);
    nAttr.setKeyable(false);
    nAttr.setHidden(true);
    addAttribute(locatorSX);

    locatorSY = nAttr.create("locatorSY", "locatorSY", MFnNumericData::kDouble);
    nAttr.setStorable(false);
    nAttr.setKeyable(false);
    nAttr.setHidden(true);
    addAttribute(locatorSY);

    drawPercent = nAttr.create("drawPercent", "drawPercent", MFnNumericData::kDouble);
    nAttr.setStorable(false);
    nAttr.setKeyable(false);
    nAttr.setDefault(100.);
    nAttr.setMin(0.);
    nAttr.setMax(100.);
    addAttribute(drawPercent);
    
    lastPointWeight = nAttr.create("lastPointWeight", "lastPointWeight", MFnNumericData::kDouble);
    nAttr.setStorable(false);
    nAttr.setKeyable(false);
    addAttribute(lastPointWeight);

    lastPoint = nAttr.createPoint("lastPoint", "lastPoint");
    nAttr.setStorable(false);
    nAttr.setKeyable(false);
    addAttribute(lastPoint);
  
    MFnDoubleArrayData doubleArrayDataFn;
    doubleArrayDataFn.create(&stat);
    _CheckStatus(stat, (MString("Unable to create doubleArray: ") + stat.errorString()));
    sampledWeights = tAttr.create("sampledWeights", "sampledWeights", MFnData::kDoubleArray, doubleArrayDataFn.object(), &stat);
    _CheckStatus(stat, (MString("Unable to create sampledWeights attr: ") + stat.errorString()));
    tAttr.setHidden(true);
    tAttr.setKeyable(false);
    tAttr.setStorable(true);
    addAttribute(sampledWeights);

    resample = nAttr.create("resample", "resample", MFnNumericData::kBoolean, true);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    addAttribute(resample);

    attributeAffects(traceImageFilename, aspectRatio);
    attributeAffects(traceImageFilename, aspectRatio1);

    attributeAffects(traceImageFilename, tracedGeom);
    attributeAffects(traceCurve,  tracedGeom);
    attributeAffects(traceStep,   tracedGeom);
    attributeAffects(lineWidth,   tracedGeom);
    attributeAffects(locatorTX,   tracedGeom);
    attributeAffects(locatorTY,   tracedGeom);
    attributeAffects(locatorSX,   tracedGeom);
    attributeAffects(locatorSY,   tracedGeom);
    attributeAffects(drawPercent, tracedGeom);

    attributeAffects(traceImageFilename, lastPointWeight);
    attributeAffects(traceCurve,  lastPointWeight);
    attributeAffects(traceStep,   lastPointWeight);
    attributeAffects(locatorTX,   lastPointWeight);
    attributeAffects(locatorTY,   lastPointWeight);
    attributeAffects(locatorSX,   lastPointWeight);
    attributeAffects(locatorSY,   lastPointWeight);
    attributeAffects(drawPercent, lastPointWeight);

    attributeAffects(traceImageFilename, lastPoint);
    attributeAffects(traceCurve,  lastPoint);
    attributeAffects(traceStep,   lastPoint);
    attributeAffects(locatorTX,   lastPoint);
    attributeAffects(locatorTY,   lastPoint);
    attributeAffects(locatorSX,   lastPoint);
    attributeAffects(locatorSY,   lastPoint);
    attributeAffects(drawPercent, lastPoint);

    return MS::kSuccess;
}

MStatus EngraveItNode::compute( const MPlug& plug, MDataBlock& data )
{
    MStatus stat;
    if( (plug != tracedGeom) && (plug != aspectRatio) && (plug != lineWidth) 
        && (plug != aspectRatio1) && (plug != traceStep) && (plug != traceImageFilename) 
        && (plug != trashold)) return MS::kUnknownParameter;

    MDataHandle outTrGeoHnd      = data.outputValue(EngraveItNode::tracedGeom);
    MDataHandle outAspectHnd     = data.outputValue(EngraveItNode::aspectRatio);
    MDataHandle outAspect1Hnd    = data.outputValue(EngraveItNode::aspectRatio1);
    MDataHandle outLastPoint        = data.outputValue(EngraveItNode::lastPoint);
    MDataHandle outLastPointWeight  = data.outputValue(EngraveItNode::lastPointWeight);
    MDataHandle inStepHnd        = data.inputValue(EngraveItNode::traceStep);
    MDataHandle inResampleHnd    = data.inputValue(EngraveItNode::resample);
    MDataHandle inLineWidth      = data.inputValue(EngraveItNode::lineWidth);
    MDataHandle inTrashold       = data.inputValue(EngraveItNode::trashold);
    MArrayDataHandle inCurvesHnd = data.inputArrayValue(EngraveItNode::traceCurve);
    MDataHandle inImgFnameHnd    = data.inputValue(EngraveItNode::traceImageFilename);
    MDataHandle inCurveHnd;

    MDataHandle locTxHnd = data.inputValue(EngraveItNode::locatorTX);
    MDataHandle locTyHnd = data.inputValue(EngraveItNode::locatorTY);
    _locTX = locTxHnd.asDouble();
    _locTY = locTyHnd.asDouble();
    
    MDoubleArray outSampledWeights, inSampledWeights;
    MFnDoubleArrayData doubleArrayDataFn;

    MString imageFilenameStr = inImgFnameHnd.asString();
    bool    resample = inResampleHnd.asBool();
    double  step = inStepHnd.asDouble();
    double  maxWidth = inLineWidth.asDouble();
    double  trash = inTrashold.asDouble();
    double  _drawPercent = data.inputValue(EngraveItNode::drawPercent).asDouble();
    unsigned int stopAtSample = 0, totalSamples =0;
    unsigned int inSamples = 0; 

    if (!resample)
    {
        sampleImageVariants = E_STORED;
        MObject _tmp = data.inputValue(EngraveItNode::sampledWeights).data();
        stat = doubleArrayDataFn.setObject(_tmp);
        _CheckStatusReturn(stat, (MString("Unable to (1) for inSampledWeights: ") + stat.errorString()));
        inSampledWeights = doubleArrayDataFn.array();
        inSamples = inSampledWeights.length();
    }
    else
    {
        sampleImageVariants = E_NORM;
        MObject _tmp = data.outputValue(EngraveItNode::sampledWeights).data();
        stat = doubleArrayDataFn.setObject(_tmp);
        _CheckStatusReturn(stat, (MString("Unable to (1) for outSampledWeights: ") + stat.errorString()));
        outSampledWeights = doubleArrayDataFn.array();
        outSampledWeights.clear();
    }

    MImage image;
    unsigned int _w, _h, pixelStride;
    if (imageFilenameStr.length() != 0 && image.readFromFile(imageFilenameStr) == MStatus::kSuccess)
    {
        image.getSize(_w, _h);
        pixelStride = image.depth();
    }
    else
    {
        sampleImageVariants = E_ONE;
        _Error("Cant load file");
    }
    
    double  _aspect = 1;
    _aspect = double(_w) / _h;
    outAspectHnd.set(_aspect);
    outAspect1Hnd.set(1.);
    outAspectHnd.setClean();
    outAspect1Hnd.setClean();

    double tStart, tEnd, shadowStep, len;
    double last_coll_sample, coll_sample = 0, lineWidth = 0, _last_weight = 0;
    
    MPointArray _verts;
    MIntArray _counts, _inds;
    MPoint pt, pt1, pt2, last_pt;
    MVector curveNormal;
    uint storageIndex = 0, last_ind;

    MMatrix worldToLocTrans = getWorldToLocatorTrans();
    const unsigned int nElems = inCurvesHnd.elementCount();
    
    //calculate total number of samples 
    for (unsigned int i = 0; i < nElems; i++)
    {
        inCurvesHnd.jumpToElement(i);
        inCurveHnd = inCurvesHnd.inputValue();
        MFnNurbsCurve curveFn(inCurveHnd.asNurbsCurve());
        len = curveFn.length();
        totalSamples += len / step;
    }
    stopAtSample = ceil(totalSamples*_drawPercent*0.01);

    for (unsigned int i = 0; i < nElems; i++)
    {
        inCurvesHnd.jumpToElement(i);
        inCurveHnd = inCurvesHnd.inputValue();

        MFnNurbsCurve curveFn(inCurveHnd.asNurbsCurve(), &stat);
        len = curveFn.length();
        curveFn.getKnotDomain(tStart, tEnd);
        shadowStep = (tEnd - tStart)*step / len;
        last_coll_sample = 1.1; // prevent jumps on multiple curves, can be done with distanse check also.
        double t = tStart;
        int number_of_steps = len / step;

        for (int i=0; i < number_of_steps; i++, t+=shadowStep, storageIndex++)
        {

            curveFn.getPointAtParam(t, pt, MSpace::kWorld);
            if (sampleImageVariants != E_STORED)
            {
                coll_sample = sampleImageAt(worldToLocTrans * pt, image.pixels(), pixelStride, _w, _h, _aspect, 1.);
                outSampledWeights.append(coll_sample);
            }
            else
            {
                if(storageIndex < inSamples) coll_sample = inSampledWeights[storageIndex];
                else
                {
                    coll_sample = 0;
                    MGlobal::displayWarning(MString("Too little storred weights at: ") + storageIndex);
                }
            }

            if (storageIndex > stopAtSample) continue;

            curveNormal = curveFn.normal(t, MSpace::kWorld);
            curveNormal.normalize();
            lineWidth = maxWidth * (1 - coll_sample);
            pt1 = pt + curveNormal*lineWidth;
            pt2 = pt - curveNormal*lineWidth;
            last_ind = _verts.length() - 1;
            _last_weight = 1 - coll_sample;

            // TODO: Test jumps on curve end?
            if (t == tStart)
            {
                last_coll_sample = 1.;
            }
            else if (coll_sample < trash && last_coll_sample >= trash)
            {
                //   *
                // *     
                //   *
                _verts.append(last_pt);
                _verts.append(pt1);
                _verts.append(pt2);
                _inds.append(last_ind + 1); _inds.append(last_ind + 2); _inds.append(last_ind + 3);
                _counts.append(3);
                last_coll_sample = coll_sample;
            }
            else if (coll_sample >= trash && last_coll_sample < trash)
            {
                // *
                //   *     
                // *
                _verts.append(pt);
                _inds.append(last_ind + 1); _inds.append(last_ind); _inds.append(last_ind - 1);
                _counts.append(3);
                last_coll_sample = coll_sample;
            }
            else if (coll_sample < trash && last_coll_sample < trash)
            {
                // *  *
                //        
                // *  *
                _verts.append(pt1); _verts.append(pt2);
                _inds.append(last_ind - 1); _inds.append(last_ind);
                _inds.append(last_ind + 2); _inds.append(last_ind + 1);
                _counts.append(4);
                last_coll_sample = coll_sample;
            }
            else
            {
                last_coll_sample = 1.;
            }
            last_pt = pt;
        }
    }

    MFnMesh meshFn;
    MFnMeshData meshDataFn;
    MObject newMeshData = meshDataFn.create();
  
    meshFn.create(_verts.length(), _counts.length(), _verts, _counts, _inds, newMeshData, &stat);
    _CheckStatusReturn(stat, (MString("Unable to create mesh: ")+stat.errorString()));
    meshFn.updateSurface();
    outTrGeoHnd.set(newMeshData);
    outTrGeoHnd.setClean();

    if (sampleImageVariants != E_STORED) doubleArrayDataFn.set(outSampledWeights);
    outLastPointWeight.set(_last_weight);
    outLastPointWeight.setClean();

    MFloatVector& _last_pt = outLastPoint.asFloatVector();
    _last_pt = last_pt;
    outLastPoint.setClean();

    data.setClean( plug );
    return MS::kSuccess;
}

MMatrix EngraveItNode::getWorldToLocatorTrans()
{
    MStatus stat;
    MPlugArray connectedPlgs;
    MObject thisNodeObj = thisMObject();
    MFnDependencyNode thisNodeFn(thisNodeObj);
    MPlug plg = thisNodeFn.findPlug(aspectRatio);
    plg.connectedTo(connectedPlgs, false, true);
    MDagPath dagPath;
    MFnDagNode dagFn(connectedPlgs[0].node());
    dagFn.getPath(dagPath);
    MMatrix worldToLocTrans = dagPath.inclusiveMatrixInverse(&stat);
    _CheckStatus(stat, "Could not get transform matrix to locator space");

    MFnTransform transFn(connectedPlgs[0].parent(0));
    MEulerRotation rot;
    transFn.getRotation(rot);
    worldToLocTrans *= rot.asMatrix().inverse();
    worldToLocTrans.matrix[0][3] -= _locTX;
    worldToLocTrans.matrix[1][3] -= _locTY;
    
    return worldToLocTrans;
}

void *EngraveItNode::creator()
{
    return new EngraveItNode();
}
