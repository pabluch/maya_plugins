#include "BasicLocator.h"
#include <maya/MGlobal.h>
#include <maya/MPointArray.h>
#include <maya/MFnDagNode.h>
#include "helpers.h"

const MTypeId BasicLocator::typeId(0x0012acc1);
const MString BasicLocator::typeName("basicLocator");

MObject BasicLocator::width;
MObject BasicLocator::height;

bool BasicLocator::getPoints( MPointArray &pts ) const
{
    MStatus stat;
    MObject thisNode = thisMObject();
    MFnDagNode dagFn( thisNode );  

    MPlug widthPlug     = dagFn.findPlug( width,  &stat);
    _CheckStatus(stat, "_width plug failed");
    MPlug heightPlug    = dagFn.findPlug( height, &stat);
    _CheckStatus(stat, "_height plug failed");
    float _width, _height;
   
    widthPlug.getValue(_width);
    heightPlug.getValue(_height);

    _width/= 2.;
    _height/=2.;
    
    pts.clear(); 
    pts.setSizeIncrement( 4 );
   
    MPoint pt; pt.z = 0.0;
   
	 pt.x = -_width; pt.y = _height;
	 pts.append( pt );

    pt.x = _width; pt.y = _height;
    pts.append(pt);

    pt.x = _width; pt.y = -_height;
    pts.append(pt);

    pt.x = -_width; pt.y = -_height;
    pts.append(pt);

    pt.x = -_width; pt.y = _height;
    pts.append(pt);

    return true;
}

void BasicLocator::draw( M3dView & view, const MDagPath & path, M3dView::DisplayStyle style, M3dView::DisplayStatus status )
{   
    view.beginGL(); 
    glPushAttrib(GL_CURRENT_BIT);
    MPointArray pts;
    getPoints(pts);

    glBegin(GL_LINE_STRIP);
    for( unsigned int i=0; i < pts.length(); i++ )
        glVertex3f( float(pts[i].x), float(pts[i].y), float(pts[i].z) );
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(-0.25f, 0.0f, 0.0f);
        glVertex3f(0.25f, 0.0f, 0.0f);

        glVertex3f(0.0f, 0.0f, -0.25f);
        glVertex3f(0.0f, 0.0f, 0.25f);
    glEnd();

    glPopAttrib();
    view.endGL();
}

bool BasicLocator::isBounded() const
{ 
    return true;
}

MBoundingBox BasicLocator::boundingBox() const
{   
    MPointArray pts;
    getPoints( pts );

    MBoundingBox bbox;
    for( unsigned int i=0; i < pts.length(); i++ )
	    bbox.expand( pts[i] );
    return bbox;
}

void *BasicLocator::creator()
{
    return new BasicLocator();
}

MStatus BasicLocator::initialize()
{ 
    MFnUnitAttribute unitFn;    
    MFnNumericAttribute numFn;
    MStatus stat;
    
    width = unitFn.create( "width", "width", MFnUnitAttribute::kDistance );
    unitFn.setDefault( MDistance(1.0, MDistance::uiUnit()) );
    unitFn.setMin( MDistance(0.0, MDistance::uiUnit()) );
    unitFn.setKeyable( true );
    stat = addAttribute( width );
    _CheckStatusReturn(stat, "cant add attr width");

    height = unitFn.create( "height", "height", MFnUnitAttribute::kDistance );
    unitFn.setDefault( MDistance(1.0, MDistance::uiUnit()) );
    unitFn.setMin( MDistance(0.0, MDistance::uiUnit()) );
    unitFn.setKeyable( true );
    stat = addAttribute( height );
    _CheckStatusReturn(stat, "cant add attr height");



    return MS::kSuccess;
}