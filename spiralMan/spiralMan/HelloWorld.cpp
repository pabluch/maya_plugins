#include <maya/MPxCommand.h>
#include <maya/MGlobal.h>
#include <maya/MFnPlugin.h>

#define  __ProgramName "[HalloWorld]"
#include "helpers.h"


//#include <iostream>
//#include <maya/MSimple.h>
//#include <maya/MIOStream.h>
//DeclareSimpleCommand(HelloWorld, "Autodesk", "2016");
//
//MStatus HelloWorld::doIt(const MArgList&)
//{
//	std::cout << "Hello World\n" << std::endl;
//	return MS::kSuccess;
//}

class HelloWorld2Cmd : public MPxCommand
{
public:
	virtual MStatus	doIt(const MArgList&) 
	{ 
		MGlobal::displayInfo("Hello World\n"); 
		_CheckBool(0);
		return MS::kSuccess; 
	}
	virtual MStatus	undoIt(const MArgList&)
	{
		MGlobal::displayInfo("Hello World undo\n");
		
		return MS::kSuccess;
	}
	static void *creator() 
	{ 
		return new HelloWorld2Cmd; 
	}
};

MStatus initializePlugin(MObject obj)
{
	MFnPlugin plugin(obj, "Pawel Lukaszewciz", "1.0");

	MStatus stat;
	stat = plugin.registerCommand("helloWorld2", HelloWorld2Cmd::creator);
	if (!stat)
		stat.perror("registerCommand failed");

	return stat;
}

MStatus uninitializePlugin(MObject obj)
{
	MFnPlugin plugin(obj);

	MStatus	stat;
	stat = plugin.deregisterCommand("helloWorld2");
	if (!stat)
		stat.perror("deregisterCommand failed");

	return stat;
}




