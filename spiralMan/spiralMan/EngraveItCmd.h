#pragma once
#include <maya/MPxCommand.h>
#include <maya/MDagModifier.h>
#include <maya/MDGModifier.h>

class EngraveItCmd : public MPxCommand 
{
public:
	virtual MStatus doIt( const MArgList& );
	virtual MStatus undoIt();
 	virtual MStatus redoIt();
	virtual bool isUndoable() const 
   { 
       return true; 
   }
	static void *creator() 
   { 
       return new EngraveItCmd(); 
   }

private:
   MDagModifier dagMod[2];
};
